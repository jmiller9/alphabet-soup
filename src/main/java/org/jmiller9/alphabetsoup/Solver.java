package org.jmiller9.alphabetsoup;

import java.util.Map;
import java.util.HashMap;
import java.util.List;
import java.util.ArrayList;


/**
 * This class is responsible for solving a wordsearch
 * 
 */
public class Solver {
    private Map<Character, ArrayList<Node>> letterMap;
    private List<String> words;

    public Solver(Map<Character, ArrayList<Node>> letterMap, List<String> words) {
        this.letterMap = letterMap;
        this.words = words;
    }

    /**
     * Recursively solve a substring of an individual word
     * 
     * @param node The letter we are currently searching on
     * @param word Substring of the word to search for
     * @param direction Direction to traverse the wordsearch in
     * @return Node indicating there was a solution, or null
     */
    private Node solveSubword(Node node, String word, Direction direction) {
        if (node.getLetter() == word.charAt(0)) {
            if (word.length() == 1) {
                // This is the end node for our solution
                return node;
            }
            else {
                Node adjacent = node.getAdjacentNode(direction);
                if (adjacent != null) {
                    Node solutionEndNode = solveSubword(adjacent, word.substring(1), direction);
                    if (solutionEndNode != null) {
                        // We have found a solution, no need to continue
                        return solutionEndNode;
                    }
                }
            }
        }
        // No solution was found on this path
        return null;
    }

    /**
     * Solve an individual word in the wordsearch
     * 
     * @param word The word to search for
     * @param solutionMap Map of word : solution
     */
    private void solveWord(String word, Map<String, String> solutionMap) {
        List<Node> candidates = letterMap.get(word.charAt(0));
        for (Node node : candidates) {
            for (Direction direction : Direction.values()) {
                Node solutionEndNode = solveSubword(node, word, direction);
                if (solutionEndNode != null) {
                    // This word has a solution
                    String solution = node.toString() + " " + solutionEndNode.toString();
                    solutionMap.put(word, solution);
                    return;
                }
            }
        }
        // No solution could be found
        solutionMap.put(word, "No solution");
    }

    /**
     * Solves the wordsearch
     * 
     * @return map of word : solution
     */
    public Map<String, String> solve() {
        Map<String, String> solutionMap = new HashMap<>();
        for (String word : words) {
            solveWord(word, solutionMap);
        }
        return solutionMap;
    }
}