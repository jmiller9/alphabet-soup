package org.jmiller9.alphabetsoup;

import java.util.Map;
import java.util.HashMap;
import java.util.Set;
import java.util.List;
import java.util.ArrayList;


/**
 * This class is responsible for parsing file contents,
 * setting Node adjacencies in our letter graph,
 * and returning a map of letter : list of nodes along with
 * a list of words to search for.
 * 
 * The map parse() returns serves as a shortcut to narrow down the
 * search space when trying to find a letter (Node) to start on.
 * 
 */
public class Parser {
    /**
     * Parse the header and extract the number of rows and columns
     * 
     * @param header String formatted as something like "5x5" or "6x3"
     * @return Tuple containing rows, columns
     */
    private Tuple<Integer, Integer> parseHeader(String header) {
        String[] dimensionStrs = header.split("x");
        int rows = Integer.parseInt(dimensionStrs[0]);
        int columns = Integer.parseInt(dimensionStrs[1]);
        return new Tuple<>(rows, columns);
    }

    /**
     * Sets adjacencies for all Nodes in the grid
     * 
     * @param rows list of lists of Nodes
     */
    private void setAdjacencies(ArrayList<ArrayList<Node>> rows) {
        for (int rowIndex = 0; rowIndex < rows.size(); rowIndex++) {
            for (int columnIndex = 0; columnIndex < rows.get(rowIndex).size(); columnIndex++) {
                Node node = rows.get(rowIndex).get(columnIndex);
                if (rowIndex - 1 >= 0) {
                    // North
                    node.addAdjacentNode(rows.get(rowIndex-1).get(columnIndex), Direction.NORTH);
                    if (columnIndex - 1 >= 0) {
                        // Northwest
                        node.addAdjacentNode(rows.get(rowIndex-1).get(columnIndex-1), Direction.NORTHWEST);
                    }
                    if (columnIndex + 1 < rows.get(rowIndex).size()) {
                        // Northeast
                        node.addAdjacentNode(rows.get(rowIndex-1).get(columnIndex+1), Direction.NORTHEAST);
                    }
                }
                if (rowIndex + 1 < rows.size()) {
                    // South
                    node.addAdjacentNode(rows.get(rowIndex+1).get(columnIndex), Direction.SOUTH);
                    if (columnIndex - 1 >= 0) {
                        // Southwest
                        node.addAdjacentNode(rows.get(rowIndex+1).get(columnIndex-1), Direction.SOUTHWEST);
                    }
                    if (columnIndex + 1 < rows.get(rowIndex).size()) {
                        // Southeast
                        node.addAdjacentNode(rows.get(rowIndex+1).get(columnIndex+1), Direction.SOUTHEAST);
                    }
                }
                if (columnIndex + 1 < rows.get(rowIndex).size()) {
                    // East
                    node.addAdjacentNode(rows.get(rowIndex).get(columnIndex+1), Direction.EAST);
                }
                if (columnIndex - 1 >= 0) {
                    // West
                    node.addAdjacentNode(rows.get(rowIndex).get(columnIndex-1), Direction.WEST);
                }
            }
        }
    }

    /**
     * Create a blank letter map
     * This is used as a shortcut for where to start looking for words
     * Extracts list of words sans spaces/newlines, all in upper-case
     * 
     * @return map of letter:empty list for all letters in the alphabet
     */
    private Map<Character, ArrayList<Node>> generateLetterMap() {
        Map<Character, ArrayList<Node>> map = new HashMap<>();
        char[] alphabet = {
            'A',
            'B',
            'C',
            'D',
            'E',
            'F',
            'G',
            'H',
            'I',
            'J',
            'K',
            'L',
            'M',
            'N',
            'O',
            'P',
            'Q',
            'R',
            'S',
            'T',
            'U',
            'V',
            'W',
            'X',
            'Y',
            'Z'
        };
        for (char letter : alphabet) {
            ArrayList<Node> nodes = new ArrayList<>();
            map.put(letter, nodes);
        }
        return map;
    }

    /**
     * Parse the list of lines
     * Creates a map of letter:Nodes; each Node will have its adjacencies set
     * Extracts list of words sans spaces/newlines, all in upper-case
     * 
     * @param lines list of lines read from a text file
     * @return map of letter:Nodes, list of words obtained from the file
     */
    public Tuple<Map<Character, ArrayList<Node>>, List<String>> parse(List<String> lines) {
        boolean first = true;
        int rowIndex = 0;
        int numRows = 0;
        List<String> words = new ArrayList<>();
        ArrayList<ArrayList<Node>> rows = new ArrayList<>();
        // This will help us narrow down where to start looking for solutions
        Map<Character, ArrayList<Node>> letterMap = generateLetterMap();
        // Set containing all letters A-Z. We could have used a regex,
        // but we get this for free after generating the letterMap
        Set<Character> allLetters = letterMap.keySet();
        for (String line : lines) {
            if (first) {
                Tuple<Integer, Integer> header = parseHeader(line);
                numRows = header.getFirst();
                first = false;
            }
            else {
                if (rowIndex < numRows) {
                    // Build our Nodes
                    String[] rawRow = line.split(" ");
                    ArrayList<Node> row = new ArrayList<>();
                    int columnIndex = 0;
                    for (String rawLetter : rawRow) {
                        // Get rid of trailing new lines and convert to uppercase
                        char letter = rawLetter.toUpperCase().charAt(0);
                        // Make sure we aren't putting non-letter characters in
                        if (allLetters.contains(letter)) {
                            Node node = new Node(letter, rowIndex, columnIndex);
                            row.add(node);
                            letterMap.get(letter).add(node);
                            columnIndex++;
                        }
                    }
                    rows.add(row);
                }
                else {
                    // Populate our word list
                    String word = line.replace(" ", "").replace("/r", "").replace("/n", "").toUpperCase();
                    words.add(word);
                }
                rowIndex++;
            }
        }
        setAdjacencies(rows);
        return new Tuple<>(letterMap, words);
    }    
}