package org.jmiller9.alphabetsoup;

import java.io.IOException;
import java.io.BufferedReader;
import java.io.FileReader;
import java.util.Scanner;
import java.util.List;
import java.util.ArrayList;
import java.util.Map;

/**
 * Solves wordsearch puzzles
 *
 */
public class App 
{
    /**
     * Extract and return the lines of an ASCII file in a List of Strings
     * 
     * @param filepath
     * @return lines of an ASCII file as a List of Strings
     */
    private static List<String> readLines(String filepath) throws IOException {
        List<String> lines = new ArrayList<>();
        try (
            Scanner scanner = new Scanner(new BufferedReader(new FileReader(filepath)));
        ) {
            while (scanner.hasNextLine()) {
                lines.add(scanner.nextLine());
            }
        }

        return lines;
    }
    
    /**
     * Reads an ASCII file that defines a wordsearch and solves it
     * Wordsearch results are printed to stdout
     * @param filepath
     */
    public static void readAndSolve(String filepath) throws IOException {
        List<String> lines = readLines(filepath);
        Parser parser = new Parser();
        Tuple<Map<Character, ArrayList<Node>>, List<String>> parserTuple = parser.parse(lines);
        Map<Character, ArrayList<Node>> letterNodeMap = parserTuple.getFirst();
        List<String> words = parserTuple.getSecond();
        Solver solver = new Solver(letterNodeMap, words);
        Map<String, String> solutionMap = solver.solve();
        for (String word : words) {
            String solution = solutionMap.get(word);
            if (solution != null) {
                System.out.println(word + " " + solution);
            }
        }
    }

    /**
     * Main method
     * Entry point into this application
     * @param args command-line arguments
     */
    public static void main(String[] args)
    {
        if (args.length > 0) {
            String filepath = args[0];
            try {
                readAndSolve(filepath);
            }
            catch (IOException e) {
                System.err.println("Could not parse file " + filepath);
            }
        }
        else {
            System.err.println("filepath must be supplied as a command line argument");
        }
    }
}
