package org.jmiller9.alphabetsoup;

import java.util.EnumMap;


/**
 * Represents a letter and its 2D coordinates in a wordsearch
 * This class also contains a map of all adjacent letters,
 * keyed by their Direction relative to this Node
 */
public class Node {
    private char letter;
    private int row;
    private int column;
    private EnumMap<Direction, Node> adjacencies;

    public Node(char letter, int row, int column) {
        this.letter = letter;
        this.row = row;
        this.column = column;
        adjacencies = new EnumMap<>(Direction.class);
    }

    /**
     * Get the letter this node represents at position row,column
     * 
     * @return the letter this Node represents at its position
     */
    public char getLetter() {
        return letter;
    }

    /**
     * Get the row coordinate of this Node
     * 
     * @return the row this Node is located in
     */
    public int getRow() {
        return row;
    }

    /**
     * Get the column coordinate of this Node
     * 
     * @return the column this Node is located in
     */
    public int getColumn() {
        return column;
    }

    /**
     * Add a Node representing a letter adjacent to this Node
     * 
     * @param node Node representing an adjacent letter
     * @param direction Direction of adjacency
     */
    public void addAdjacentNode(Node node, Direction direction) {
        adjacencies.put(direction, node);
    }

    /**
     * Get a Node adjacent to this Node
     * 
     * @param direction Direction of adjacency
     * @return Node adjacent to this Node, or null if no such adjacency exists
     */
    public Node getAdjacentNode(Direction direction) {
        return adjacencies.get(direction);
    }

    /**
     * Overridden toString() method
     * This is a unique identifier of a Node, as
     * two Nodes will never exist in the same space.
     * 
     * @return String in the format row:column e.g. "4:3"
     */
    @Override
    public String toString() {
        return row + ":" + column;
    }

    /**
     * Overridden hashCode() method
     * 
     * @return hash of this Node's unique identifier
     */
    @Override
    public int hashCode() {
        return toString().hashCode();
    }

    /**
     * Overridden equals() method
     * Checks to see if the Nodes' rows and columns match
     * 
     * @return equality of the Nodes
     */
    @Override
    public boolean equals(Object other) {
        Node otherNode = (Node)other;
        if (otherNode != null) {
            return row == otherNode.row && column == otherNode.column;
        }
        return false;
    }
}

