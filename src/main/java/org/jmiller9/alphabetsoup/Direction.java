package org.jmiller9.alphabetsoup;


/**
 * Represents directions of adjacencies
 * 
 */
public enum Direction {
    NORTH,SOUTH,EAST,WEST,NORTHEAST,NORTHWEST,SOUTHEAST,SOUTHWEST
}
