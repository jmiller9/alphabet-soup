package org.jmiller9.alphabetsoup;

/**
 * Class that mimics a Python tuple with a fixed length of 2 elements.
 * This is essentially just a pair of objects of potentially varying types.
 * 
 */
public class Tuple<T, V> {
    private T first;
    private V second;

    public Tuple(T first, V second) {
        this.first = first;
        this.second = second;
    }

    /**
     * Get the first Object in the Tuple
     * 
     * @return Object of type T
     */
    public T getFirst() {
        return first;
    }

    /**
     * Get the second Object in the Tuple
     * 
     * @return Object of type V
     */
    public V getSecond() {
        return second;
    }
}
