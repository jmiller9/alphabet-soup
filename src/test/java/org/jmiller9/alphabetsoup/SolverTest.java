package org.jmiller9.alphabetsoup;

import static org.junit.Assert.assertEquals;

import org.junit.Test;

import java.util.List;
import java.util.Arrays;
import java.util.Map;
import java.util.ArrayList;

public class SolverTest {
    private List<String> getTestData() {
        String[] data = new String[] {
            "4x4",
            "W X R F",
            "O A L U",
            "E X L L",
            "M A I L",
            "MEOW",
            "WALL",
            "EAR",
            "MAIL",
            "ILL",
            "FULL",
            "Z",
            "MAX",
            "W XR F",
        };
        return Arrays.asList(data);
    }

    @Test
    public void testSolver()
    {
        Parser parser = new Parser();
        Tuple<Map<Character, ArrayList<Node>>, List<String>> parserResults = parser.parse(getTestData());
        Map<Character, ArrayList<Node>> letterMap = parserResults.getFirst();
        List<String> words = parserResults.getSecond();
        Solver solver = new Solver(letterMap, words);
        Map<String, String> resultsMap = solver.solve();
        // Make sure we get our expected results
        assertEquals("3:0 0:0", resultsMap.get("MEOW"));
        assertEquals("0:0 3:3", resultsMap.get("WALL"));
        assertEquals("2:0 0:2", resultsMap.get("EAR"));
        assertEquals("3:0 3:3", resultsMap.get("MAIL"));
        assertEquals("3:2 1:2", resultsMap.get("ILL"));
        assertEquals("0:3 3:3", resultsMap.get("FULL"));
        // There is no 'Z' character in the grid
        assertEquals("No solution", resultsMap.get("Z"));
        // "MAX" would be available if we could change directions, but for word searches we cannot do this
        assertEquals("No solution", resultsMap.get("MAX"));
        // W XR F should condense to WXRF
        assertEquals("0:0 0:3", resultsMap.get("WXRF"));
    }
}
