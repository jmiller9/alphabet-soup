package org.jmiller9.alphabetsoup;

import static org.junit.Assert.assertEquals;

import org.junit.Test;

import java.util.List;
import java.util.Arrays;
import java.util.Map;
import java.util.ArrayList;

public class ParserTest {
    private List<String> getTestData() {
        String[] data = new String[] {
            "3x3",
            "A B C",
            "D E F",
            "G H A",
            "ABC",
        };
        return Arrays.asList(data);
    }


    @Test
    public void testParser()
    {
        Parser parser = new Parser();
        Tuple<Map<Character, ArrayList<Node>>, List<String>> parserResults = parser.parse(getTestData());
        Map<Character, ArrayList<Node>> letterMap = parserResults.getFirst();
        List<String> words = parserResults.getSecond();
        // The test data contains only one word
        assertEquals(1, words.size());
        assertEquals("ABC", words.get(0));
        // There is a second 'A' character on purpose
        assertEquals(2, letterMap.get('A').size());
        // Only one 'B', though
        assertEquals(1, letterMap.get('B').size());
        // No Z's
        assertEquals(0, letterMap.get('Z').size());
        // All letters are present in the map, whether they have nodes or not
        assertEquals(26, letterMap.keySet().size());
        // There should be one Node for "C"
        assertEquals(1, letterMap.get('C').size());
        Node cNode = letterMap.get('C').get(0);
        // cNode should only have 3 non-null adjacencies ('F', 'B', and 'E')
        assertEquals(null, cNode.getAdjacentNode(Direction.NORTH));
        assertEquals('F', cNode.getAdjacentNode(Direction.SOUTH).getLetter());
        assertEquals(null, cNode.getAdjacentNode(Direction.EAST));
        assertEquals('B', cNode.getAdjacentNode(Direction.WEST).getLetter());
        assertEquals(null, cNode.getAdjacentNode(Direction.NORTHEAST));
        assertEquals(null, cNode.getAdjacentNode(Direction.NORTHWEST));
        assertEquals(null, cNode.getAdjacentNode(Direction.SOUTHEAST));
        assertEquals('E', cNode.getAdjacentNode(Direction.SOUTHWEST).getLetter());
    }
}
